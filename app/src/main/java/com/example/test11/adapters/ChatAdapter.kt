package com.example.test11.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.test11.R
import com.example.test11.databinding.ChatItemBinding
import com.example.test11.extensions.setImage
import com.example.test11.model.ChatInfo
import java.util.concurrent.TimeUnit

class ChatAdapter : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {


    val items = mutableListOf<ChatInfo>()


    inner class ViewHolder(private val binding: ChatItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: ChatInfo
        fun onBind() {
            model = items[adapterPosition]
            binding.userIconBtn.setImage(model.avatar)
            val hours = TimeUnit.MILLISECONDS.toHours(model.updatedDate)
            val hoursMillis = TimeUnit.HOURS.toMillis(hours)
            val minutes =  TimeUnit.MILLISECONDS.toMinutes(model.updatedDate - hoursMillis)
            binding.timeTV.text = "$hours:$minutes PM"
            if (model.isTyping)
                binding.isWriting.isVisible = true
            else
                binding.unreadMsg.apply {
                    isVisible = true
                    setText(model.unreaMessage)
                }
            binding.nameTV.text = model.firstName + model.lastName
            if (model.messageType == "voice"){
                binding.MsgTypeTV.apply {
                    isVisible = true
                    text = "Sent a voice message"
                }
                binding.msgIcon.apply {
                    isVisible = true
                    setImageResource(R.drawable.ic_voice)
                }
            }
            if (model.messageType == "attachment"){
                binding.MsgTypeTV.apply {
                    isVisible = true
                    text = "Sent an attachment"
                }
                binding.msgIcon.apply {
                    isVisible = true
                    setImageResource(R.drawable.ic_attachement)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ChatItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()

    }

    override fun getItemCount() = items.size

    fun setData(newList: MutableList<ChatInfo>) {
        items.clear()
        items.addAll(newList)
        notifyDataSetChanged()

    }
}
