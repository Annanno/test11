package com.example.test11.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.test11.R

fun ImageView.setImage(url: String?){
    if (!url.isNullOrEmpty())
        Glide.with(context).load(url).error(R.mipmap.ic_launcher).into(this)
    else
        setImageResource(R.mipmap.ic_launcher)
}