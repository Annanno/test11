package com.example.test11


import com.example.test11.model.ChatInfo
import com.example.test11.network.ApiServiceImpl

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

import retrofit2.Response
import javax.inject.Inject


class ChatRepository @Inject constructor(private val apiServiceImpl: ApiServiceImpl): Data() {

    suspend fun getInfo(): Flow<Resource<ChatInfo>> = flow {

            emit(handleResponse(request = {apiServiceImpl.getInfo()}, "Something went wrong"))

    }


}

abstract class Data {
    suspend fun <T> handleResponse(
        request: suspend () -> Response<T>,
        defaultMessage: String
    ): Resource<T> {

        return try {
            val result = request.invoke()
            val body = result.body()

            if (result.isSuccessful && body != null) {
                return Resource.Success(body)
            } else {
                // Only for hackers -> Here you can parse error body
                Resource.Error(result.message() ?: defaultMessage)
            }
        } catch (e: Exception) {
            Resource.Error("Something went wrong!", null)
        }
    }
}





