package com.example.test11.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.test11.ChatRepository
import com.example.test11.Resource
import com.example.test11.model.ChatInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChatViewModel @Inject constructor(private val repository: ChatRepository) : ViewModel() {

    private val _chatResponse = MutableStateFlow<Resource<ChatInfo>>(Resource.Empty())
    val chatResponse: StateFlow<Resource<ChatInfo>> = _chatResponse


    fun getInfo() {
        viewModelScope.launch {
            repository.getInfo().collect {
                _chatResponse.emit(it)
            }

        }
    }


}