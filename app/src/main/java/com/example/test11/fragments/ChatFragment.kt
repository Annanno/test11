package com.example.test11.fragments



import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.test11.viewmodel.ChatViewModel
import com.example.test11.Resource
import com.example.test11.adapters.ChatAdapter
import com.example.test11.base.BaseFragment
import com.example.test11.databinding.FragmentChatBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


@AndroidEntryPoint
class ChatFragment : BaseFragment<FragmentChatBinding>(FragmentChatBinding::inflate) {

    private lateinit var chatAdapter: ChatAdapter
    private val viewModel: ChatViewModel by viewModels()

    override fun init() {
        initRecycler()
        collectData()
    }

    private fun initRecycler() {
        chatAdapter = ChatAdapter()
        binding.recycler.apply {
            adapter = chatAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun collectData(){
        viewModel.getInfo()
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                viewModel.chatResponse.collect {
                    when(it){
                        is Resource.Loading ->{

                        }
                        is Resource.Error -> {
                            Toast.makeText(requireContext(), "Something wrong", Toast.LENGTH_SHORT).show()
                        }
                        is Resource.Success ->{

                            chatAdapter.setData(mutableListOf(it.data!!))
                        }
                        is Resource.Empty -> {

                        }
                    }
                }
            }
        }

    }


}