package com.example.test11.network

import javax.inject.Inject

class ApiServiceImpl  @Inject constructor(private val apiService: ApiService) {
    suspend fun getInfo() =
        apiService.getInfo()
}
