package com.example.test11

sealed class Resource<out T>(
    val data: T? = null,
    val loading: Boolean = false,
    val message: String? = null,
) {
    class Loading<T>(isLoading: Boolean) : Resource<T>(loading = isLoading)
    class Error<T>(message: String, data: T? = null) : Resource<T>(message = message, data = data)
    class Success<T>(data: T) : Resource<T>(data = data)
    class Empty<T> : Resource<T>(null, false, null )
}